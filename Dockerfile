# Imagen raiz
# FROM gacomun/polymer_base

# Install Polymer CLI, https://www.polymer-project.org/3.0/docs/tools/polymer-cli
FROM gacomun/polymer_base

# RUN git clone https://gacomun@bitbucket.org/gacomun/bankjco_web.git


# Carpeta raiz
WORKDIR /bankjco_web

# Copia de archivos de carpeta local apitechu
ADD . /bankjco_web

# Instalación de las dependencias (se ejecuta mientras se esta construyendo)
RUN npm install --only=prod

# Puerto de trabajo
EXPOSE 8080

CMD ["polymer", "serve"]
