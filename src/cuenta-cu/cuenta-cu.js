import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../bankjco-notificacion/bankjco-notificacion.js'

/**
 * @customElement
 * @polymer
 */
class CuentaCU extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Cuentas</li>
          <li class="breadcrumb-item active" aria-current="page">[[modo]]</li>
        </ol>
      </nav>
      <h4>[[modo]] de Cuenta</h4><br/>
      <div>
        <div class="row">
          <div class="col-11">
            <input type="text" class="form-control" value="{{IBAN::input}}" placeholder="*IBAN"/>
          </div>
          <div class="col-1">
            <button type="button" class="form-control" on-click="actionIBAN">
              <img src="img/red.svg" width="20"  data-toggle="tooltip" data-placement="top" title="Generador"/>
            </button>
          </div>
        </div>
        <div class="row">
          <div class="col-11">
            <input type="text" class="form-control" value="{{balance::input}}" placeholder="Saldo"/>
          </div>
        </div>
        <div class="row">
          <div class="col-11">
            <input type="text" class="form-control" value="{{currency::input}}" placeholder="Divisa" disabled/>
          </div>
        </div>
        <div class="row">
          <div class="col-1">
            <button on-click="action" type="button" class="btn btn-warning form-control"><b>Ok</b></button>
          </div>
          <div class="col-1">
            <button on-click="cancelar" type="button" class="btn btn-secondary form-control"><b>Cancel</b></button>
          </div>
        </div>
      </div>
      <iron-ajax
        id="getAccount"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]/accounts/[[idaccount]]"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
      <iron-ajax
        id="postAccount"
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]/accounts/"
        headers=[[token]]
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="showOperation"
        on-error="showError"
      >
      </iron-ajax>
      <iron-ajax
        id="putAccount"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]/accounts/[[idaccount]]"
        handle-as="json"
        content-type="application/json"
        method="PUT"
        on-response="showOperation"
        on-error="showError"
      >
      </iron-ajax>
      <iron-ajax
        id="getIBAN"
        url="https://api.mockaroo.com/api/d1e0aa80?count=1&key=e50c8500"
        handle-as="json"
        on-response="showDataIBAN"
        on-error="showErrorIBAN"
      >
      </iron-ajax>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
    `;
  }
  static get properties() {
    return {
      modo: {
        type: String,
        observer: "_modoChanged" //convencion el "_"
      },
      IBAN: {
        type: String
      },
      balance: {
        type: Number
      },
      currency: {
        type: String,
        value: "EUR"
      },
      created: {
        type: String
      },
      iduser: {
        type: Number
      },
      idaccount: {
        type: Number,
        observer: "_accountidChanged" //convencion el "_"
      },
      token:{
        type: Object,
        value: {"X-tokensec": sessionStorage.getItem('X-tokensec')}
      }
    };
  }//end properties
  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.IBAN=data.detail.response.IBAN;
    this.balance=data.detail.response.balance;
    this.currency=data.detail.response.currency;
  }

  showError(data){
    console.log("showError");
    console.log(data.detail.request.xhr.response);
    this.$.notificacion.errorMsg=data.detail.request.xhr.response.msg;
    this.$.notificacion.tipo=2;
    this.$.notificacion.open();

  }
  _modoChanged(newValue,oldValue){
    console.log(`Value has changed ${oldValue} -> ${newValue}`);
    if(this.modo==="Alta"){
      this.IBAN="";
      this.balance=0;
      this.currency="EUR";
      this.created="";
    }

  }
  _accountidChanged(newValue,oldValue){
    console.log(`Value has changed ${oldValue} -> ${newValue}`);
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    this.$.getAccount.generateRequest();
  }
  action(e){
    console.log("El usuario ha pulsado el boton");
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    var AccountData={
      "IBAN" :this.IBAN,
      "balance":this.balance,
      "currency" :this.currency
    }
    console.log(JSON.stringify(AccountData));
    if(this.modo==="Alta"){
      this.$.postAccount.body= JSON.stringify(AccountData);
      this.$.postAccount.generateRequest();
    }else if (this.modo==="Modificacion") {
      this.$.putAccount.body= JSON.stringify(AccountData);
      this.$.putAccount.generateRequest();
    }else{
      this.$.notificacion.errorMsg="Incorrect access mode";
      this.$.notificacion.tipo=2;
      this.$.notificacion.open();
    }
  }
  actionIBAN(e){
    console.log("El usuario ha pulsado el boton");
    this.$.getIBAN.generateRequest();
  }
  showOperation(data){
    console.log("showOperation");
    console.log(data.detail.response);
    this.dispatchEvent(
      new CustomEvent(
        "cuevent", // si ponemos en mayusculas no funciona
        {
          "detail":{ //detail es obligatorio
              "msg" : "Done correctly",
              "tipo" : 1
          }
        }
      )
    );
    this.IBAN="";
    this.balance="0";
    this.currency="EUR";

  }
  showDataIBAN(data){
    console.log("showDataIBAN");
    console.log(data.detail.response);
    this.IBAN=data.detail.response.IBAN;
  }

  showErrorIBAN(data){
    console.log("showDataIBAN");
    console.log(data.detail.request.xhr.response);
    this.$.notificacion.errorMsg=data.detail.request.xhr.response.error;
    this.$.notificacion.tipo=2;
    this.$.notificacion.open();
  }
  cancelar(e){
    console.log("cancelar");
    this.dispatchEvent(
      new CustomEvent(
        "cancelevent", // si ponemos en mayusculas no funciona
        {}
      )
    );
  }

}//end class

window.customElements.define('cuenta-cu', CuentaCU);
