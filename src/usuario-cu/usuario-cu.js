import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../bankjco-notificacion/bankjco-notificacion.js'

/**
 * @customElement
 * @polymer
 */
class UsuarioCU extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h4>[[modo]] de Usuario</h4><br/>
      <input type="text" value="{{first_name::input}}" placeholder="Nombre"/><br/>
      <input type="text" value="{{middle_name::input}}" placeholder="Apellidos 1"/><br/>
      <input type="text" value="{{last_name::input}}" placeholder="Apellidos 2"/><br/>
      <input type="text" value="{{age::input}}" placeholder="Edad"/><br/>
      <input type="range" min="18" value="{{age::input}}" max="150"/><br/>
      <input id="emailform" type="email" value="{{email::input}}" placeholder="*Email@"/><br/>
      <input id="pwdform" type="password" value="{{password::input}}" placeholder="*Password"/><br/>
      <button on-click="action" type="button" class="btn btn-warning"><b>Ok</b></button>
      <button on-click="cancelar" type="button" class="btn btn-secondary"><b>Cancel</b></button>
      <iron-ajax
        id="getUser"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
      <iron-ajax
        id="postUser"
        url="http://localhost:3000/bankjco/v1/users/"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="showOperation"
        on-error="showError"
      >
      </iron-ajax>
      <iron-ajax
        id="putUser"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]"
        handle-as="json"
        content-type="application/json"
        method="PUT"
        on-response="showOperation"
        on-error="showError"
      >
      </iron-ajax>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
    `;
  }
  static get properties() {
    return {
      modo: {
        type: String,
        observer: "_modoChanged" //convencion el "_"
      },
      first_name: {
        type: String
      },
      middle_name: {
        type: String
      },
      last_name: {
        type: String
      },
      age: {
        type: Number
      },
      email: {
        type: String
      },
      created: {
        type: String
      },
      password: {
        type: String
      },
      iduser: {
        type: String,
        observer: "_useridChanged" //convencion el "_"
      },
      errorMsg: {
        type: String
      },
      token:{
        type: Object,
        value: {"X-tokensec": sessionStorage.getItem('X-tokensec')}
      }
    };
  }//end properties
  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.first_name=data.detail.response.first_name;
    this.middle_name=data.detail.response.middle_name;
    this.last_name=data.detail.response.last_name;
    this.age=data.detail.response.age;
    this.email=data.detail.response.email;
    var fecha=new Date(data.detail.response.created);
    // this.created=fecha.getDay()+"/"+fecha.getMonth()+"/"+fecha.getFullYear();
    this.created=fecha;
    this.password=data.detail.response.password;

  }

  showError(data){
    console.log("showError");
    console.log(data.detail.request.xhr.response);
    this.$.notificacion.errorMsg=data.detail.request.xhr.response.msg;
    this.$.notificacion.tipo=2;
    this.$.notificacion.open();

  }
  _useridChanged(newValue,oldValue){
    console.log(`Value has changed ${oldValue} -> ${newValue}`);
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    this.$.getUser.generateRequest();
  }
  _modoChanged(newValue,oldValue){
    console.log(`_modoChanged ${oldValue} -> ${newValue}`);
    if (this.modo==="Modificacion") {
      this.$.emailform.disabled=true;
      this.$.pwdform.hidden=true;
    }else{
      this.$.emailform.disabled=false;
      this.$.pwdform.hidden=false;
    }
  }
  action(e){
    console.log("El usuario ha pulsado el boton");
    var UserData={
      "first_name" :this.first_name,
      "middle_name":this.middle_name,
      "last_name" :this.last_name,
      "age" :this.age,
      "email" :this.email,
      "password" :this.password
    }
    console.log(JSON.stringify(UserData));
    if(this.modo==="Alta"){
      this.$.postUser.body= JSON.stringify(UserData);
      this.$.postUser.generateRequest();
    }else if (this.modo==="Modificacion") {
      this.$.putUser.body= JSON.stringify(UserData);
      this.$.putUser.generateRequest();
    }else{
      this.$.notificacion.errorMsg="Incorrect access mode";
      this.$.notificacion.tipo=2;
      this.$.notificacion.open();
    }
  }
  showOperation(data){
    console.log("showOperation");
    console.log(data.detail.response);
    this.dispatchEvent(
      new CustomEvent(
        "cuevent", // si ponemos en mayusculas no funciona
        {
          "detail":{ //detail es obligatorio
              "msg" : "Done correctly",
              "tipo" : 1
          }
        }
      )
    );


  }
  cancelar(e){
    console.log("cancelar");
    this.dispatchEvent(
      new CustomEvent(
        "cancelevent", // si ponemos en mayusculas no funciona
        {}
      )
    );
  }

}//end class

window.customElements.define('usuario-cu', UsuarioCU);
