import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-selection-column.js';
import '../bankjco-notificacion/bankjco-notificacion.js'


/**
 * @customElement
 * @polymer
 */
class UsuarioListado extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <vaadin-grid items="[[userItems]]" theme="row-stripes">
        <vaadin-grid-selection-column auto-select frozen></vaadin-grid-selection-column>
        <vaadin-grid-column path="first_name" header="Nombre"></vaadin-grid-column>
        <vaadin-grid-column header="Apellidos"><template>[[item.middle_name]] [[item.last_name]]</template></vaadin-grid-column>
        <vaadin-grid-column path="age" header="Edad"></vaadin-grid-column>
        <vaadin-grid-column path="email" header="Email"></vaadin-grid-column>
        <vaadin-grid-column path="rol" header="Rol"></vaadin-grid-column>
        <vaadin-grid-column path="created" header="Fecha Ingreso"></vaadin-grid-column>
      </vaadin-grid>
      <iron-ajax
        auto
        id="listUsers"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
    `;
  }
  static get properties() {
    return {
      userItems: {
        type: Array
      },
      errorMsg: {
        type: String
      },
      token:{
        type: Object,
        value: {"X-tokensec": sessionStorage.getItem('X-tokensec')}
      }
    };
  }//end properties
  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.userItems=data.detail.response;
  }

  showError(data){
    console.log("showError");
    console.log(data.detail.request.xhr.response);
    this.$.notificacion.errorMsg=data.detail.request.xhr.response.msg;
    this.$.notificacion.tipo=2;
    this.$.notificacion.open();
  }
}//end class

window.customElements.define('usuario-listado', UsuarioListado);
