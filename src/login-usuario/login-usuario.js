import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../bankjco-notificacion/bankjco-notificacion.js'

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <img src="img/casa.svg" class="img-fluid mx-auto d-block" width="50%">
      <input type="email" class="form-control" placeholder="email" value="{{email::input}}"/>
      <br/>
      <input type="password" class="form-control" placeholder="password" value="{{password::input}}"/>
      <br/>
      <button on-click="login" type="button" class="btn btn-warning btn-block"><b>Login</b></button>
      <button on-click="singup" type="button" class="btn btn-primary btn-block"><b>Sign up</b></button>
      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/bankjco/v1/sessions"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAjaxResponse"
        on-error="showError"
      >
      </iron-ajax>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
    `;
  }
  static get properties() {
    return {
      password: {
        type: String
      },
      email: {
        type: String
      }
    };
  }//end properties

  login(e){
    console.log("El usuario ha pulsado el boton");

    var loginData={
      "email": this.email,
      "password": this.password
    }
    this.$.doLogin.body= JSON.stringify(loginData);
    this.$.doLogin.generateRequest();
    // console.log("login"+loginData); // No
    // console.log(loginData); //Ok
  }
  singup(e){
    console.log("El usuario ha pulsado el boton singup");

    this.dispatchEvent(
      new CustomEvent(
        "singupevent", // si ponemos en mayusculas no funciona
        {}
      )
    );
  }
  // manageAjaxResponse(data, request){
  manageAjaxResponse(data,request){
    console.log("Llegaron los resultados");
    var token = request.xhr.getResponseHeader("X_tokensec");
    console.log(token);
    console.log(data.detail.response);
    // sessionStorage.setItem('X-tokensec', token);
    sessionStorage.setItem('X-tokensec', data.detail.response.X_tokensec);
    this.dispatchEvent(
      new CustomEvent(
        "loginevent", // si ponemos en mayusculas no funciona
        {
          "detail":{ //detail es obligatorio
            "userId" : data.detail.response._id
          }
        }
      )
    );
  }

  showError(error){
    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.request.xhr.response);
    this.$.notificacion.errorMsg=error.detail.request.xhr.response.msg;
    this.$.notificacion.tipo=2;
    this.$.notificacion.open();
  }

}//end class

window.customElements.define('login-usuario', LoginUsuario);
