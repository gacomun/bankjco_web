import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../visor-usuario/visor-usuario.js'
import '../visor-cuenta/visor-cuenta.js'

/**
 * @customElement
 * @polymer
 */
class BankjcoMenu extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <div >
        <div class="row">
          <div class="col">
            <span hidden$="{{!showListAccount}}"><visor-cuenta id="visorcuenta" iduser=[[iduser]]></visor-cuenta></span>
          </div>
          <div class="col-2">
            <visor-usuario on-loginevent="loginEvent" on-logoutevent="logoutEvent"></visor-usuario>

          </div>
        </div>
      </div>

    `;
  }
  static get properties() {
    return {
      showListAccount: {
        type: Boolean,
        value: false
      },
      iduser: {
        type: String
      }
    };
  }//end properties
  loginEvent(e){
    console.log("Capturado evento de login Main");
    console.log(e);
    this.showListAccount=true;
    this.iduser=e.detail.userId;
    this.$.visorcuenta.componentName="listado";
  }
  logoutEvent(e){
    console.log("Capturado evento de logout Main");
    console.log(e);
    this.showListAccount=false;
  }

}//end class

window.customElements.define('bankjco-menu', BankjcoMenu);
