import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '../bankjco-notificacion/bankjco-notificacion.js'
import '../bankjco-utils/bankjco-utils.js'


/**
 * @customElement
 * @polymer
 */
class CuentaListado extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <!-- <vaadin-grid items="[[accountItems]]" theme="wrap-cell-content row-stripes"> -->
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">Cuentas</li>
        </ol>
      </nav>

      <div>
        <div class="row">
          <div class="col">
            <vaadin-grid items="[[accountItems]]" theme="column-borders row-stripes" height-by-rows on-active-item-changed="itemchanged" id="gridaccount">
              <vaadin-grid-column path="IBAN" header="IBAN" width="60%"></vaadin-grid-column>
              <vaadin-grid-column path="balance" header="Saldo" width="16%"></vaadin-grid-column>
              <vaadin-grid-column path="currency" header="Divisa" width="8%"></vaadin-grid-column>
              <vaadin-grid-column path="created" header="Fecha Creación" width="16%"></vaadin-grid-column>
            </vaadin-grid>
          </div>
          <div class="col-1">
            <button type="button" on-click="alta">
              <img src="img/add-file.svg" width="20"  data-toggle="tooltip" data-placement="top" title="Nuevo"/>
            </button><br/>
            <button id="dimg" type="button" on-click="detalle" disabled>
              <img src="img/lupa.svg" width="20"  data-toggle="tooltip" data-placement="top" title="detalle"/>
            </button><br/>
            <button id="mimg" type="button" on-click="update" disabled>
              <img src="img/editar.svg" width="20" data-toggle="tooltip" data-placement="top" title="Modificacion"/>
            </button><br/>
            <button id="movimg" type="button" on-click="movimiento" disabled>
              <img src="img/lista.svg" width="20"data-toggle="tooltip" data-placement="top" title="Movimientos"/>
            </button><br/>
          </div>
        </div>
      </div>
      <iron-ajax
        id="listAccounts"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]/accounts"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
      <bankjco-utils id="utils" ></bankjco-utils>
    `;
  }
  static get properties() {
    return {
      accountItems: {
        type: Array,
        observer: "_accountsChanged" //convencion el "_"
      },
      errorMsg: {
        type: String
      },
      iduser: {
        type: String,
        observer: "_useridChanged" //convencion el "_"
      },
      token:{
        type: Object,
        value: {"X-tokensec": sessionStorage.getItem('X-tokensec')}
      }
    };
  }//end properties
  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.accountItems=data.detail.response;
  }

  showError(data){
    console.log("showError");
    console.log(data.detail.request.xhr.response);
    this.$.notificacion.errorMsg=data.detail.request.xhr.response.msg;
    this.$.notificacion.tipo=2;
    this.$.notificacion.open();
  }
  _useridChanged(newValue,oldValue){
    console.log(`_useridChanged ${oldValue} -> ${newValue}`);
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    this.$.listAccounts.generateRequest();
  }
  _accountsChanged(newValue,oldValue){
    console.log(`_accountsChanged ${oldValue} -> ${newValue}`);
    var account={};
    for (account of newValue){
      account.created=this.$.utils.formatDate(new Date(account.created));
    };
  }
  click(e){
    console.log("El usuario ha pulsado el boton click");
    console.log(e);
    console.log(this.$.gridaccount.selectedItems);
  }
  clickdetalle(idaccount){
    console.log("El usuario ha pulsado el boton click");
    console.log(idaccount);
  }
  itemchanged(event){
    console.log("itemchanged");
    const item = event.detail.value;
    console.log(item);
    this.$.gridaccount.selectedItems = item ? [item] : [];
    this.$.dimg.disabled = item ? false : true;
    this.$.mimg.disabled = item ? false : true;
    this.$.movimg.disabled = item ? false : true;
  }
  alta(e){
    console.log("El usuario ha pulsado el boton alta");
    this.dispatchEvent(
      new CustomEvent(
        "altacuentaevent", // si ponemos en mayusculas no funciona
        {}
      )
    );
  }
  update(e){
    console.log("El usuario ha pulsado el boton update");
    this.dispatchEvent(
      new CustomEvent(
        "updateevent", // si ponemos en mayusculas no funciona
        {
            "detail":{ //detail es obligatorio
                "iduser" : this.iduser,
                "idaccount" : this.$.gridaccount.selectedItems[0]._id
            }
        }
      )
    );
  }
  detalle(e){
    console.log("El usuario ha pulsado el boton detalle");
    this.dispatchEvent(
      new CustomEvent(
        "detalleevent", // si ponemos en mayusculas no funciona
        {
            "detail":{ //detail es obligatorio
                "iduser" : this.iduser,
                "idaccount" : this.$.gridaccount.selectedItems[0]._id
            }
        }
      )
    );
  }
  movimiento(e){
    console.log("El usuario ha pulsado el boton movimiento");
    this.dispatchEvent(
      new CustomEvent(
        "movimientoevent", // si ponemos en mayusculas no funciona
        {
            "detail":{ //detail es obligatorio
                "iduser" : this.iduser,
                "idaccount" : this.$.gridaccount.selectedItems[0]._id,
                "IBAN" : this.$.gridaccount.selectedItems[0].IBAN
            }
        }
      )
    );
  }
  refresh(){
    console.log(`refresh`);
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    this.$.gridaccount.selectedItems = [];
    this.$.dimg.disabled = true;
    this.$.mimg.disabled =  true;
    this.$.movimg.disabled =  true;
    this.$.listAccounts.generateRequest();
  }


}//end class

window.customElements.define('cuenta-listado', CuentaListado);
