import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../bankjco-notificacion/bankjco-notificacion.js'

/**
 * @customElement
 * @polymer
 */
class Template extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <iron-ajax
        auto
        id="listUsers"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
    `;
  }
  static get properties() {
    return {
    };
  }//end properties
  showData(data){
    console.log("showData");
    console.log(data.detail.response);
  }
  showError(data){
    console.log("showError");
    console.log(data.detail.request.xhr.response);
    this.$.notificacion.errorMsg=data.detail.request.xhr.response.msg;
    this.$.notificacion.tipo=2;
    this.$.notificacion.open();
  }
}//end class

window.customElements.define('template', Template);
