import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../bankjco-notificacion/bankjco-notificacion.js'
import '../bankjco-utils/bankjco-utils.js'

/**
 * @customElement
 * @polymer
 */
class CuentaRD extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Cuentas</li>
          <li class="breadcrumb-item active" aria-current="page">Detalle</li>
        </ol>
      </nav>
      <b>IBAN :</b> [[IBAN]]<br/>
      <b>Saldo :</b> [[balance]]<br/>
      <b>Divisa :</b> [[currency]]<br/>
      <b>Fecha de Ingreso :</b> [[created]]<br/>
      <button on-click="cancelar" type="button" class="btn btn-secondary"><b>Cancel</b></button>
      <button on-click="delete" type="button" class="btn btn-danger"><b>Borrar</b></button>
      <iron-ajax
        id="getAccount"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]/accounts/[[idaccount]]"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
      <iron-ajax
        id="deleteAccount"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]/accounts/[[idaccount]]"
        handle-as="json"
        method="DELETE"
        on-response="showDataDelete"
        on-error="showError"
      >
      </iron-ajax>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
      <bankjco-utils id="utils" ></bankjco-utils>
    `;
  }
  static get properties() {
    return {
      IBAN: {
        type: String
      },
      balance: {
        type: Number
      },
      currency: {
        type: String
      },
      created: {
        type: String
      },
      iduser: {
        type: Number
      },
      idaccount: {
        type: Number,
        observer: "_accountidChanged" //convencion el "_"
      },
      token:{
        type: Object,
        value: {"X-tokensec": sessionStorage.getItem('X-tokensec')}
      }
    };
  }//end properties
  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.IBAN=data.detail.response.IBAN;
    this.balance=data.detail.response.balance;
    this.currency=data.detail.response.currency;
    var fecha=new Date(data.detail.response.created);
    // this.created=fecha.getDay()+"/"+fecha.getMonth()+"/"+fecha.getFullYear();
    this.created=this.$.utils.formatDate(fecha);


  }
  showDataDelete(data){
    console.log("showDataDelete");
    console.log(data.detail.response);
    this.dispatchEvent(
      new CustomEvent(
        "deleteevent", // si ponemos en mayusculas no funciona
        {
          "detail":{ //detail es obligatorio
              "msg" : "Delete done correctly",
              "tipo" : 1
          }
        }
      )
    );
  }

  showError(data){
    console.log("showError");
    console.log(data.detail.request.xhr.response);
    this.$.notificacion.errorMsg=data.detail.request.xhr.response.msg;
    this.$.notificacion.tipo=2;
    this.$.notificacion.open();
  }
  _accountidChanged(newValue,oldValue){
    console.log(`_accountidChanged ${oldValue} -> ${newValue}`);
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    this.$.getAccount.generateRequest();
  }
  delete(e){
    console.log("El usuario ha pulsado el boton Borrar");
    this.$.deleteAccount.generateRequest();
  }
  cancelar(e){
    console.log("cancelar");
    this.dispatchEvent(
      new CustomEvent(
        "cancelevent", // si ponemos en mayusculas no funciona
        {}
      )
    );
  }

}//end class

window.customElements.define('cuenta-rd', CuentaRD);
