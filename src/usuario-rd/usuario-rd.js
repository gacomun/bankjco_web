import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../bankjco-notificacion/bankjco-notificacion.js'
import '../bankjco-utils/bankjco-utils.js'

/**
 * @customElement
 * @polymer
 */
class UsuarioRD extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <b>Nombre :</b><br/>
      [[first_name]]<br/>
      <b>Apellidos :</b><br/>
      [[middle_name]] [[last_name]]<br/>
      <b>Edad :</b><br/>
      [[age]]<br/>
      <b>Email :</b><br/>
      [[email]]<br/>
      <b>Fecha de Ingreso :</b><br/>
      [[created]]<br/>
      <button on-click="logout" type="button" class="btn btn-warning btn-block"><b>Logout</b></button>
      <button on-click="delete" type="button" class="btn btn-danger btn-block"><b>Borrar</b></button>
      <button on-click="update" type="button" class="btn btn-primary btn-block"><b>Modificar</b></button>
      <iron-ajax
        id="getUser"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
      <iron-ajax
        id="deleteUser"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]"
        handle-as="json"
        method="DELETE"
        on-response="showDataDelete"
        on-error="showError"
      >
      <iron-ajax
        id="deleteSession"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/sessions/[[iduser]]"
        handle-as="json"
        method="DELETE"
        on-response="sessionDelete"
        on-error="showError"
      >
      </iron-ajax>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
      <bankjco-utils id="utils" ></bankjco-utils>
    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      },
      middle_name: {
        type: String
      },
      last_name: {
        type: String
      },
      age: {
        type: Number
      },
      email: {
        type: String
      },
      created: {
        type: String
      },
      iduser: {
        type: String,
        observer: "_useridChanged" //convencion el "_"
      },
      token:{
        type: Object,
        value: {"X-tokensec": sessionStorage.getItem('X-tokensec')}
      }
    };
  }//end properties
  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.first_name=data.detail.response.first_name;
    this.middle_name=data.detail.response.middle_name;
    this.last_name=data.detail.response.last_name;
    this.age=data.detail.response.age;
    this.email=data.detail.response.email;
    var fecha=new Date(data.detail.response.created);
    this.created=this.$.utils.formatDate(fecha);
    // this.created=fecha;

  }
  showDataDelete(data){
    console.log("showDataDelete");
    console.log(data.detail.response);
    this.dispatchEvent(
      new CustomEvent(
        "deleteevent", // si ponemos en mayusculas no funciona
        {
          "detail":{ //detail es obligatorio
              "msg" : "Delete done correctly",
              "tipo" : 1
          }
        }
      )
    );
  }
  sessionDelete(data){
    console.log("sessionDelete");
    console.log(data.detail.response);
    sessionStorage.removeItem('X-tokensec');
    this.dispatchEvent(
      new CustomEvent(
        "logoutevent", // si ponemos en mayusculas no funciona
        {
          "detail":{ //detail es obligatorio
              "msg" : "Logout done correctly",
              "tipo" : 1
          }
        }
      )
    );

  }

  showError(data){
    console.log("showError");
    console.log(data.detail.request.xhr.response);
    this.$.notificacion.errorMsg=data.detail.request.xhr.response.msg;
    this.$.notificacion.tipo=2;
    this.$.notificacion.open();
  }
  _useridChanged(newValue,oldValue){
    console.log(`_useridChanged ${oldValue} -> ${newValue}`);
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    this.$.getUser.generateRequest();
  }
  delete(e){
    console.log("El usuario ha pulsado el boton Borrar");
    this.$.deleteUser.generateRequest();
  }
  logout(e){
    console.log("El usuario ha pulsado el boton logout");
    this.$.deleteSession.generateRequest();
  }
  update(e){
    console.log("El usuario ha pulsado el boton update");
    this.dispatchEvent(
      new CustomEvent(
        "updateevent", // si ponemos en mayusculas no funciona
        {
            "detail":{ //detail es obligatorio
              "iduser" : this.iduser
            }
        }
      )
    );
  }
  refresh(){
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    this.$.getUser.generateRequest();
  }

}//end class

window.customElements.define('usuario-rd', UsuarioRD);
