import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages'
import '../login-usuario/login-usuario.js'
import '../usuario-cu/usuario-cu.js'
import '../usuario-rd/usuario-rd.js'
import '../bankjco-notificacion/bankjco-notificacion.js'

/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
        <div component-name="login"><login-usuario on-loginevent="loginEvent" on-singupevent="processSingupEvent"></login-usuario></div>
        <div component-name="alta"><usuario-cu id="cuusuario" on-cuevent="processCUEvent" on-cancelevent="cancelEvent"></usuario-cu></div>
        <div component-name="consulta"><usuario-rd id="rdusuario" on-deleteevent="logoutEvent" on-logoutevent="logoutEvent" on-updateevent="processUpdateEvent"></usuario-rd></div>
      </iron-pages>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
    `;
  }
  static get properties() {
    return {
      componentName: {
        type: String,
        value: "login"
      }
    };
  }//end properties
  _componentNameChanged(newValue,oldValue){
    console.log(`Value has changed ${oldValue} -> ${newValue}`);
  }
  processSingupEvent(e){
    console.log("Capturado evento de singup");
    console.log(e);
    this.componentName="alta";
    this.$.cuusuario.modo="Alta"
  }
  processCUEvent(e){
    console.log("Capturado evento de Alta/Update");
    console.log(e);
    if(sessionStorage.getItem('X-tokensec'))
    {
      this.componentName="consulta";
      this.$.rdusuario.refresh();
      this.$.notificacion.errorMsg=e.detail.msg;
      this.$.notificacion.tipo=e.detail.tipo;
      this.$.notificacion.open();
    }else{
      this.componentName="login";
      this.$.notificacion.errorMsg=e.detail.msg;
      this.$.notificacion.tipo=e.detail.tipo;
      this.$.notificacion.open();
    }
  }
  logoutEvent(e){
    console.log("Capturado evento de logout");
    console.log(e);
    this.componentName="login";
    this.$.notificacion.errorMsg=e.detail.msg;
    this.$.notificacion.tipo=e.detail.tipo;
    this.$.notificacion.open();
    this.dispatchEvent(
      new CustomEvent(
        "logoutevent", // si ponemos en mayusculas no funciona
        {
        }
      )
    );
  }
  cancelEvent(e){
    console.log("Capturado evento de Cancelar");
    console.log(e);
    if(sessionStorage.getItem('X-tokensec'))
    {
      this.componentName="consulta";
    }else{
      this.componentName="login";
    }
  }
  loginEvent(e){
    console.log("Capturado evento de login");
    console.log(e);
    this.componentName="consulta";
    this.$.rdusuario.iduser=e.detail.userId;
    this.dispatchEvent(
      new CustomEvent(
        "loginevent", // si ponemos en mayusculas no funciona
        {
          "detail":{ //detail es obligatorio
            "userId" : e.detail.userId
          }
        }
      )
    );
  }
  processUpdateEvent(e){
    console.log("Capturado evento de update");
    console.log(e);
    this.componentName="alta";
    this.$.cuusuario.modo="Modificacion"
    this.$.cuusuario.iduser=e.detail.iduser;
  }

}//end class

window.customElements.define('visor-usuario', VisorUsuario);
