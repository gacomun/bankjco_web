import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages'
import '../cuenta-listado/cuenta-listado.js'
import '../cuenta-cu/cuenta-cu.js'
import '../cuenta-rd/cuenta-rd.js'
import '../visor-movimiento/visor-movimiento.js'
import '../bankjco-notificacion/bankjco-notificacion.js'

/**
 * @customElement
 * @polymer
 */
class VisorCuenta extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
        <div component-name="listado"><cuenta-listado id="lcuenta" iduser=[[iduser]] on-altacuentaevent="altacuenta" on-updateevent="updatecuenta" on-detalleevent="detallecuenta" on-movimientoevent="movimientocuenta"></cuenta-listado></div>
        <div component-name="alta"><cuenta-cu id="cucuenta" on-cuevent="processCUEvent" on-cancelevent="cancelEvent" iduser=[[iduser]]></cuenta-cu></div>
        <div component-name="consulta"><cuenta-rd id="rdcuenta" on-cancelevent="cancelEvent" on-deleteevent="processCUEvent" ></cuenta-rd></div>
        <div component-name="movimiento"><visor-movimiento id="vmovimiento" on-volverevent="cancelEvent"></visor-movimiento></div>
      </iron-pages>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
    `;
  }
  static get properties() {
    return {
      iduser: {
        type: String
      },
      componentName: {
        type: String,
        value: "listado"
      }
    };
  }//end properties
  altacuenta(e){
    console.log("Capturado evento de altacuenta");
    this.componentName="alta";
    this.$.cucuenta.modo="Alta"
  }
  updatecuenta(e){
    console.log("Capturado evento de updatecuenta");
    console.log(e);
    this.componentName="alta";
    this.$.cucuenta.iduser=e.detail.iduser;
    this.$.cucuenta.idaccount=e.detail.idaccount;
    this.$.cucuenta.modo="Modificacion";
  }
  detallecuenta(e){
    console.log("Capturado evento de detallecuenta");
    console.log(e);
    this.componentName="consulta";
    this.$.rdcuenta.iduser=e.detail.iduser;
    this.$.rdcuenta.idaccount=e.detail.idaccount;
    // this.$.cucuenta.modo="Modificacion";
  }
  movimientocuenta(e){
    console.log("Capturado evento de movimientocuenta");
    console.log(e);
    this.componentName="movimiento";
    this.$.vmovimiento.iduser=e.detail.iduser;
    this.$.vmovimiento.idaccount=e.detail.idaccount;
    this.$.vmovimiento.iban=e.detail.IBAN;
    // this.$.cucuenta.modo="Modificacion";
  }
  cancelEvent(e){
    console.log("Capturado evento de Cancelar");
    this.$.lcuenta.refresh();
    this.componentName="listado";
  }
  processCUEvent(e){
    console.log("Capturado evento de Alta/Update");
    this.componentName="listado";
    this.$.lcuenta.refresh();
    this.$.notificacion.errorMsg=e.detail.msg;
    this.$.notificacion.tipo=e.detail.tipo;
    this.$.notificacion.open();
  }


}//end class

window.customElements.define('visor-cuenta', VisorCuenta);
