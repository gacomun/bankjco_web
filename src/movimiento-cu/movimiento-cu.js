import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../bankjco-notificacion/bankjco-notificacion.js'
import '@fooloomanzoo/datetime-picker/overlay-datetime-picker.js';


/**
 * @customElement
 * @polymer
 */
class MovimientoCU extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Cuentas</li>
          <li class="breadcrumb-item">Movimientos</li>
          <li class="breadcrumb-item active" aria-current="page">[[modo]]</li>
        </ol>
      </nav>

      <h4>[[modo]] de Movimiento</h4><br/>
      <div>
        <div class="row">
          <div class="col-12">
            <input type="text" class="form-control" value="{{concept::input}}" placeholder="Concepto"/>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <input type="text" class="form-control" value="{{amount::input}}" placeholder="*Importe"/>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <input type="text" class="form-control" value="{{currency::input}}" placeholder="Divisa" disabled/>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
          <select class="custom-select" value="{{type::input}}">
            <option value="I">Ingreso</option>
            <option value="C">Reintegro</option>
          </select>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
          <span id="valuedateform" hidden$="false">
            <overlay-datetime-picker class="form-control" id="valuedatef" locale="es" datetime="{{value_date}}"></overlay-datetime-picker>
          </span>
          </div>
        </div>
        <div class="row">
          <div class="col-1">
            <button on-click="action" type="button" class="btn btn-warning form-control"><b>Ok</b></button>
          </div>
          <div class="col-1">
            <button on-click="cancelar" type="button" class="btn btn-secondary form-control"><b>Cancel</b></button>
          </div>
        </div>
      </div>
      <iron-ajax
        id="getTransaction"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]/accounts/[[idaccount]]/transactions/[[idtransaction]]"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
      <iron-ajax
        id="postTransaction"
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]/accounts/[[idaccount]]/transactions/"
        headers=[[token]]
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="showOperation"
        on-error="showError"
      >
      </iron-ajax>
      <iron-ajax
        id="putTransaction"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]/accounts/[[idaccount]]/transactions/[[idtransaction]]"
        handle-as="json"
        content-type="application/json"
        method="PUT"
        on-response="showOperation"
        on-error="showError"
      >
      </iron-ajax>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
    `;
  }
  static get properties() {
    return {
      modo: {
        type: String,
        observer: "_modoChanged" //convencion el "_"
      },
      concept: {
        type: String
      },
      amount: {
        type: Number
      },
      currency: {
        type: Number,
        value: "EUR"
      },
      type: {
        type: String
      },
      value_date: {
        type: Date
      },
      iduser: {
        type: Number
      },
      idaccount: {
        type: Number
      },
      idtransaction: {
        type: Number,
        observer: "_transactionidChanged" //convencion el "_"
      },
      token:{
        type: Object,
        value: {"X-tokensec": sessionStorage.getItem('X-tokensec')}
      }
    };
  }//end properties
  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.concept=data.detail.response.concept;
    this.amount=data.detail.response.amount;
    this.currency=data.detail.response.currency;
    this.type=data.detail.response.type;
    this.value_date=data.detail.response.value_date;
  }

  showError(data){
    console.log("showError");
    console.log(data.detail.request.xhr.response);
    this.$.notificacion.errorMsg=data.detail.request.xhr.response.msg;
    this.$.notificacion.tipo=2;
    this.$.notificacion.open();

  }
  _transactionidChanged(newValue,oldValue){
    console.log(`Value has changed ${oldValue} -> ${newValue}`);
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    this.$.getTransaction.generateRequest();
  }
  action(e){
    console.log("El usuario ha pulsado el boton");
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    var TransactionData={
      "concept" :this.concept,
      "amount":this.amount,
      "currency" :this.currency,
      "type" :this.type,
      "value_date" :this.value_date
    }
    console.log(JSON.stringify(TransactionData));
    if(this.modo==="Alta"){
      this.$.postTransaction.body= JSON.stringify(TransactionData);
      this.$.postTransaction.generateRequest();
    }else if (this.modo==="Modificacion") {
      this.$.putTransaction.body= JSON.stringify(TransactionData);
      this.$.putTransaction.generateRequest();
    }else{
      this.$.notificacion.errorMsg="Incorrect access mode";
      this.$.notificacion.tipo=2;
      this.$.notificacion.open();
    }
  }
  showOperation(data){
    console.log("showOperation");
    console.log(data.detail.response);
    this.dispatchEvent(
      new CustomEvent(
        "cuevent", // si ponemos en mayusculas no funciona
        {
          "detail":{ //detail es obligatorio
              "msg" : "Done correctly",
              "tipo" : 1
          }
        }
      )
    );
    this.modo=null;

    // this.concept="";
    // this.amount="";
    // this.currency="EUR";
    // this.type="";

  }
  cancelar(e){
    console.log("cancelar");
    this.dispatchEvent(
      new CustomEvent(
        "cancelevent", // si ponemos en mayusculas no funciona
        {}
      )
    );
  }
  _modoChanged(newValue,oldValue){
    console.log(`Value has changed ${oldValue} -> ${newValue}`);
    if(this.modo==="Modificacion"){
      this.$.valuedateform.hidden=false;
    }else {
      this.concept="";
      this.amount="";
      this.currency="EUR";
      this.type="";
      this.value_date=null;
      this.$.valuedateform.hidden=true;

    }

  }


}//end class

window.customElements.define('movimiento-cu', MovimientoCU);
