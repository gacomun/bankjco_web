import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';

/**
 * @customElement
 * @polymer
 */
class BankjcoNotificacion extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        .ok {
          color: #155724;
          background-color: #d4edda;
          border-color: #c3e6cb;
          position: relative;
          margin-bottom: 1rem;
          border: 1px solid transparent;
          border-radius: .25rem;
          font: italic 12pt serif;
          padding: 1em;
        }
        .error {
          color: #721c24;
          background-color: #f8d7da;
          border-color: #f5c6cb;
          position: relative;
          margin-bottom: 1rem;
          border: 1px solid transparent;
          border-radius: .25rem;
          font: italic 12pt serif;
          padding: 1em;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <vaadin-notification id="notification" position="[[position]]" duration="[[duration]]">
        <template>
          <!-- <div class="alert alert-success" role="alert"> -->
          <div id="divmsg" class$="[[clase]]">
            <div class="alert alert-danger" role="alert">
              <b>[[titulo]]</b><br>
              [[errorMsg]]
            </div>
          </div>
        </template>
      </vaadin-notification>
    `;
  }
  static get properties() {
    return {
      titulo: {
        type: String,
        value: "Error"
      },
      clase: {
        type: String,
        value: ""
      },
      tipo: {
        type: Number,
        observer: "_objChanged" //convencion el "_"
      },
      position: {
        type: String,
        value: "bottom-center"
      },
      errorMsg: {
        type: String
      },
      duration: {
        type: String,
        value: "4000"
      },
    };
  }//end properties

  _objChanged(newValue,oldValue){
    console.log(`_objChanged ${oldValue} -> ${newValue}`);
    if(this.tipo===1){
      this.clase="ok";
      this.titulo="Ok";
    }else if(this.tipo===2){
      this.clase="error"
      this.titulo="Error";
    }
    // this.$.notification.open();
  }
  open()
  {
    this.$.notification.open();
  }
}//end class

window.customElements.define('bankjco-notificacion', BankjcoNotificacion);
