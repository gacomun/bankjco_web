import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages'
import '../movimiento-listado/movimiento-listado.js'
import '../movimiento-cu/movimiento-cu.js'
import '../movimiento-rd/movimiento-rd.js'
import '../bankjco-notificacion/bankjco-notificacion.js'

/**
 * @customElement
 * @polymer
 */
class VisorMovimiento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
        <div component-name="listado">
        <movimiento-listado id="lmovimiento" iduser=[[iduser]] idaccount=[[idaccount]] iban=[[iban]]
          on-altaevent="altamovimiento"
          on-updateevent="updatemovimiento"
          on-detalleevent="detallemovimiento"
          on-volverevent="volver"
        ></movimiento-listado></div>
        <div component-name="alta"><movimiento-cu id="cumovimiento" on-cuevent="processCUEvent" on-cancelevent="cancelEvent" iduser=[[iduser]] idaccount=[[idaccount]]></movimiento-cu></div>
        <div component-name="consulta"><movimiento-rd id="rdmovimiento" on-cancelevent="cancelEvent" on-deleteevent="processCUEvent" ></movimiento-rd></div>
      </iron-pages>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
    `;
  }
  static get properties() {
    return {
      iduser: {
        type: String
      },
      idaccount: {
        type: String
      },
      iban: {
        type: String
      },
      componentName: {
        type: String,
        value: "listado"
      }
    };
  }//end properties
  altamovimiento(e){
    console.log("Capturado evento de altamovimiento");
    this.componentName="alta";
    this.$.cumovimiento.modo="Alta"
  }
  updatemovimiento(e){
    console.log("Capturado evento de updatemovimiento");
    console.log(e);
    this.componentName="alta";
    this.$.cumovimiento.iduser=e.detail.iduser;
    this.$.cumovimiento.idaccount=e.detail.idaccount;
    this.$.cumovimiento.idtransaction=e.detail.idtransaction;
    this.$.cumovimiento.modo="Modificacion";
  }
  detallemovimiento(e){
    console.log("Capturado evento de detallemovimiento");
    console.log(e);
    this.componentName="consulta";
    this.$.rdmovimiento.iduser=e.detail.iduser;
    this.$.rdmovimiento.idaccount=e.detail.idaccount;
    this.$.rdmovimiento.idtransaction=e.detail.idtransaction;
    // this.$.cucuenta.modo="Modificacion";
  }
  cancelEvent(e){
    console.log("Capturado evento de Cancelar");
    this.$.lmovimiento.refresh();
    this.componentName="listado";
  }
  processCUEvent(e){
    console.log("Capturado evento de Alta/Update");
    this.componentName="listado";
    this.$.lmovimiento.refresh();
    this.$.notificacion.errorMsg=e.detail.msg;
    this.$.notificacion.tipo=e.detail.tipo;
    this.$.notificacion.open();
  }
  volver(e){
    this.dispatchEvent(
      new CustomEvent(
        "volverevent", // si ponemos en mayusculas no funciona
        {}
      )
    );
  }



}//end class

window.customElements.define('visor-movimiento', VisorMovimiento);
