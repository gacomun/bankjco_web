import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../bankjco-notificacion/bankjco-notificacion.js'
import '../bankjco-utils/bankjco-utils.js'

/**
 * @customElement
 * @polymer
 */
class MovimientoRD extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Cuentas</li>
          <li class="breadcrumb-item">Movimientos</li>
          <li class="breadcrumb-item active" aria-current="page">Detalle</li>
        </ol>
      </nav>

      <b>Concepto :</b> [[concept]]<br/>
      <b>Importe :</b> [[amount]]<br/>
      <b>Divisa :</b> [[currency]]<br/>
      <b>Tipo :</b> [[type]]<br/>
      <b>Fecha Valor :</b> [[value_date]]<br/>
      <button on-click="cancelar" type="button" class="btn btn-secondary"><b>Cancel</b></button>
      <button on-click="delete" type="button" class="btn btn-danger"><b>Borrar</b></button>

      <iron-ajax
        id="getTransaction"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]/accounts/[[idaccount]]/transactions/[[idtransaction]]"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
      <iron-ajax
        id="deleteTransaction"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]/accounts/[[idaccount]]/transactions/[[idtransaction]]"
        handle-as="json"
        method="DELETE"
        on-response="showDataDelete"
        on-error="showError"
      >
      </iron-ajax>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
      <bankjco-utils id="utils" ></bankjco-utils>
    `;
  }
  static get properties() {
    return {
      concept: {
        type: String
      },
      amount: {
        type: Number
      },
      currency: {
        type: Number
      },
      type: {
        type: String
      },
      value_date: {
        type: String
      },
      iduser: {
        type: Number
      },
      idaccount: {
        type: Number
      },
      idtransaction: {
        type: Number,
        observer: "_transactionidChanged" //convencion el "_"
      },
      token:{
        type: Object,
        value: {"X-tokensec": sessionStorage.getItem('X-tokensec')}
      }
    };
  }//end properties
  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.concept=data.detail.response.concept;
    this.amount=data.detail.response.amount;
    this.currency=data.detail.response.currency;
    this.type=(data.detail.response.type=="I")?"Ingreso":"Reintegro";
    var fecha=new Date(data.detail.response.value_date);
    // this.created=fecha.getDay()+"/"+fecha.getMonth()+"/"+fecha.getFullYear();
    this.value_date=this.$.utils.formatDate(fecha);

  }
  showDataDelete(data){
    console.log("showDataDelete");
    console.log(data.detail.response);
    this.dispatchEvent(
      new CustomEvent(
        "deleteevent", // si ponemos en mayusculas no funciona
        {
          "detail":{ //detail es obligatorio
              "msg" : "Delete done correctly",
              "tipo" : 1
          }
        }
      )
    );
  }

  showError(data){
    console.log("showError");
    console.log(data.detail.request.xhr.response);
    this.$.notificacion.errorMsg=data.detail.request.xhr.response.msg;
    this.$.notificacion.tipo=2;
    this.$.notificacion.open();
  }
  _transactionidChanged(newValue,oldValue){
    console.log(`_transactionidChanged ${oldValue} -> ${newValue}`);
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    this.$.getTransaction.generateRequest();
  }
  delete(e){
    console.log("El usuario ha pulsado el boton Borrar");
    this.$.deleteTransaction.generateRequest();
  }
  cancelar(e){
    console.log("cancelar");
    this.dispatchEvent(
      new CustomEvent(
        "cancelevent", // si ponemos en mayusculas no funciona
        {}
      )
    );
  }

}//end class

window.customElements.define('movimiento-rd', MovimientoRD);
