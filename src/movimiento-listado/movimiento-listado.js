import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '../bankjco-notificacion/bankjco-notificacion.js'
import '../bankjco-utils/bankjco-utils.js'


/**
 * @customElement
 * @polymer
 */
class MovimientoListado extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Cuentas ([[iban]])</li>
          <li class="breadcrumb-item active" aria-current="page">Movimientos</li>
        </ol>
      </nav>

      <div>
        <div class="row">
          <div class="col">
            <vaadin-grid items="[[transactionItems]]" theme="column-borders row-stripes" height-by-rows on-active-item-changed="itemchanged" id="gridtransaction">
              <vaadin-grid-column path="concept" header="Concepto" width="20%"></vaadin-grid-column>
              <vaadin-grid-column path="amount" header="Importe" width="20%"></vaadin-grid-column>
              <vaadin-grid-column path="currency" header="Divisa" width="20%"></vaadin-grid-column>
              <vaadin-grid-column path="type" header="Tipo" width="20%"></vaadin-grid-column>
              <vaadin-grid-column path="value_date" header="Fecha Valor" width="20%"></vaadin-grid-column>
            </vaadin-grid>
            <button on-click="volver" type="button" class="btn btn-secondary"><b>Volver</b></button>
          </div>
          <div class="col-1">
            <button type="button" on-click="alta">
              <img src="img/add-file.svg" width="20"  data-toggle="tooltip" data-placement="top" title="Nuevo"/>
            </button><br/>
            <button id="dimg" type="button" on-click="detalle" disabled>
              <img src="img/lupa.svg" width="20"  data-toggle="tooltip" data-placement="top" title="detalle"/>
            </button><br/>
            <button id="mimg" type="button" on-click="update" disabled>
              <img src="img/editar.svg" width="20" data-toggle="tooltip" data-placement="top" title="Modificacion"/>
            </button><br/>
          </div>
        </div>
      </div>

      <iron-ajax
        id="listTransactions"
        headers=[[token]]
        url="http://localhost:3000/bankjco/v1/users/[[iduser]]/accounts/[[idaccount]]/transactions"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
      <bankjco-notificacion id="notificacion" ></bankjco-notificacion>
      <bankjco-utils id="utils" ></bankjco-utils>
    `;
  }
  static get properties() {
    return {
      transactionItems: {
        type: Array
      },
      errorMsg: {
        type: String
      },
      iduser: {
        type: String
      },
      idaccount: {
        type: String,
        observer: "_accountidChanged" //convencion el "_"
      },
      iban: {
        type: String
      },
      token:{
        type: Object,
        value: {"X-tokensec": sessionStorage.getItem('X-tokensec')}
      }
    };
  }//end properties
  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.transactionItems=data.detail.response;
    for (var [index,t] of this.transactionItems.entries()){
      t.type=(t.type=="I")?"Ingreso":"Reintegro";
      var fecha=new Date(t.value_date);
      t.value_date=this.$.utils.formatDate(fecha);
      this.transactionItems[index]=t;
    }
  }

  showError(data){
    console.log("showError");
    console.log(data.detail.request.xhr.response);
    this.$.notificacion.errorMsg=data.detail.request.xhr.response.msg;
    this.$.notificacion.tipo=2;
    this.$.notificacion.open();
  }
  _accountidChanged(newValue,oldValue){
    console.log(`_accountidChanged ${oldValue} -> ${newValue}`);
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    this.$.listTransactions.generateRequest();
  }
  itemchanged(event){
    console.log("itemchanged");
    const item = event.detail.value;
    console.log(item);
    this.$.gridtransaction.selectedItems = item ? [item] : [];
    this.$.dimg.disabled = item ? false : true;
    this.$.mimg.disabled = item ? false : true;
  }
  alta(e){
    console.log("El usuario ha pulsado el boton alta");
    this.dispatchEvent(
      new CustomEvent(
        "altaevent", // si ponemos en mayusculas no funciona
        {}
      )
    );
  }
  volver(e){
    console.log("El usuario ha pulsado el boton volver");
    this.dispatchEvent(
      new CustomEvent(
        "volverevent", // si ponemos en mayusculas no funciona
        {}
      )
    );
  }
  refresh(){
    console.log(`refresh`);
    this.token={"X-tokensec": sessionStorage.getItem('X-tokensec')};
    this.$.gridtransaction.selectedItems = [];
    this.$.dimg.disabled = true;
    this.$.mimg.disabled =  true;
    this.$.listTransactions.generateRequest();
  }
  detalle(e){
    console.log("El usuario ha pulsado el boton detalle");
    this.dispatchEvent(
      new CustomEvent(
        "detalleevent", // si ponemos en mayusculas no funciona
        {
            "detail":{ //detail es obligatorio
                "iduser" : this.iduser,
                "idaccount" :this.idaccount,
                "idtransaction" : this.$.gridtransaction.selectedItems[0]._id
            }
        }
      )
    );
  }
  update(e){
    console.log("El usuario ha pulsado el boton update");
    this.dispatchEvent(
      new CustomEvent(
        "updateevent", // si ponemos en mayusculas no funciona
        {
            "detail":{ //detail es obligatorio
              "iduser" : this.iduser,
              "idaccount" :this.idaccount,
              "idtransaction" : this.$.gridtransaction.selectedItems[0]._id
            }
        }
      )
    );
  }

}//end class

window.customElements.define('movimiento-listado', MovimientoListado);
