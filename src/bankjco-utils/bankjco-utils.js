import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * @customElement
 * @polymer
 */
class BankjcoUtils extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
    `;
  }
  static get properties() {
    return {
    };
  }//end properties
  formatDate(date) {
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth()+1;
    var year = date.getFullYear();
    var hour = date.getHours();
    var min = date.getMinutes();
    var segs =  date.getSeconds();

    return (day>9?"":"0")+day + '/' + (monthIndex>9?"":"0")+monthIndex + '/' + (year>9?"":"0")+year + ' ' + (hour>9?"":"0")+hour + ':' + (min>9?"":"0")+min;// + ':' + segs ;
    // return day + ' ' + monthNames[monthIndex] + ' ' + year;
  }

}//end class

window.customElements.define('bankjco-utils', BankjcoUtils);
